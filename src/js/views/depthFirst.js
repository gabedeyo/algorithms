// Node Class
class Node {
  constructor(val) {
    this.value = val;
    this.left = null;
    this.right = null;
  }
}

var inOrderArray = [];
var preOrderArray = [];
var postOrderArray= [];

// Binary Search Tree Class
class BinarySearchTree{
    constructor(){
        this.root = null;
    }

    push(val){
        var root = this.root;

        if(!root){
            this.root = new Node(val);
            return;
        }

        var currentNode = root;
        var newNode = new Node(val);

        while(currentNode){
            if(val < currentNode.value){
                if(!currentNode.left){
                    currentNode.left = newNode;
                    break;
                }else {
                    currentNode = currentNode.left;
                }
            }else{
                if(!currentNode.right){
                    currentNode.right = newNode;
                    break;
                }else{
                    currentNode = currentNode.right;
                }
            }
        }
    }

    /* Depth First Algorithms */

    // Pre Order Traversal
    preorder(node){
        if(node){
            preOrderArray.push(node.value);
            this.preorder(node.left);
            this.preorder(node.right);
        }
    }

    // In Order Traversal
    inorder(node){
        if(node){
            this.inorder(node.left);
            inOrderArray.push(node.value);
            this.inorder(node.right);
        }
    }

    // Post Order Traversal
    postorder(node){
        if(node){
            this.postorder(node.left);
            this.postorder(node.right);
            postOrderArray.push(node.value);
        }
    }
}

/* On Load Function */
(function () {
    // Get Canvas Elements Here
    var canvas = document.getElementById('testCanvas');
    var ctx = drawInitial(canvas);
    var offset = -150;

    //var hiddenValue = Math.floor(Math.random() * (array[array.length - 1] - array[0] + 1) + array[0]);

    // Display Algorithm Results Here
    var bst = new BinarySearchTree();

    drawHeading(ctx, "Binary Tree", 225, 200);
    for(let i = 0; i < 10; i++){
        var val = Math.floor(Math.random() * (100 - 0 + 1) + 0);
        bst.push(val);
        drawHeading(ctx, val, 225, offset);
        offset += 35;
    }

    bst.inorder(bst.root);

    drawHeading(ctx, "In Order", 75, 200);
    offset = -150;
    for(let i = inOrderArray.length; i > 0; i--){
        drawHeading(ctx, inOrderArray[i-1], 75, offset);
        offset += 35;
    }

    bst.preorder(bst.root);

    drawHeading(ctx, "Pre Order", -75, 200);
    offset = -150;
    for(let i = preOrderArray.length; i > 0; i--){
        drawHeading(ctx, preOrderArray[i-1], -75, offset);
        offset += 35;
    }

    bst.postorder(bst.root);

    drawHeading(ctx, "Post Order", -225, 200);
    offset = -150;
    for(let i = postOrderArray.length; i > 0; i--){
        drawHeading(ctx, postOrderArray[i-1], -225, offset);
        offset += 35;
    }

})();

//Draw Inital Canvas Elements
function drawInitial(canvas){
    let windowWidth = $(document).width();
    let windowHeight = $(document).height();

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = windowWidth / 3;
    ctx.canvas.height = windowHeight / 2;

    return ctx;
}

function drawHeading(ctx, printString, x, y){
    ctx.font="25px sans-serif";
    ctx.textAlign="center";

    ctx.fillText(printString, ctx.canvas.width/2 - x, ctx.canvas.height/2 - y);
}
