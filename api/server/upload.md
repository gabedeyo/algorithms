# File Uploads

In order to handle multipart data, first install [Multer](https://github.com/expressjs/multer).

```cmd
npm install --save multer
```

## NOS Shorthand

```javascript
    const nos = require('./modules/nos');

    // SINGLE FILE
    app.post(config.site.url + '/upload', nos.upload.single('file'), function (req, res, next) {
        console.log(req.file); // file information
        console.log(req.file.path); // temp folder path to uploaded file
    });

    // MULTIPLE FILES
    app.post(config.site.url + '/upload', nos.upload.array('file'), function (req, res, next) {
        console.log(req.files); // file information
    });
```

### Setting Options

File limit settings and the destination can be set in [config.js](../../config.js).

In the [upload.js](../../modules/upload.js) file, you can set the following options:

- **fileFilter**: function which will control which files to accept. See [the multer documentation](https://github.com/expressjs/multer#user-content-filefilter) for an example.
- **filename**: function which renames the uploaded files. See [the multer documentation](https://github.com/expressjs/multer#user-content-diskstorage)

#### Override options

If you need to override the options for a single upload, you can explicitly set them before your route.

```javascript
    const upload = require('./modules/nos/upload');

    upload.fileSize = 1000000; // in bytes
    upload.files = 1;
    upload.fileFilter = (req, file, cb) => {
      if (somecondition) {
        cb(null, false); // reject
      }
      else {
        cb(null, true); // accept
      }
    }
```

## Just Using Multer

```javascript
    const multer = require('multer');

    //CONFIGURE MULTER FILE UPLOAD LOCATION
    let storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, 'tmp/') //Your upload destination here
      },
      filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
      }
    })
    let upload = multer({ storage: storage});

    // POST for one upload only
    app.post('/api/files', upload.single('file'), function (req, res) {
       console.log(req.file);
    })

    // POST for multiple uploads
    app.post('/api/files', upload.array('file'), function (req, res) {
       console.log(req.files);
    });
```
