#Email

##Send Email Example

> Emails are sent using Nodemailr. More information can be found on their [github](https://github.com/andris9/Nodemailer).
>
> Email settings can be changed in config.js

```javascript

    //include email module
    const email = require("./modules/email");

    //build object with email information
    let emailOptions = {
            from: {
                name:'IBI Admin',
                address:'admin@ibidata.com'
            },
            to: 'msteiner@ibidata.com',
            subject: 'This is a test',
            html: '<h1>Hello World</h1>',
            log: true //indicates that this email should be saved in the database
        }

    //send email
    email.send(emailOptions,function(err, info){
        if(err) return next(err);

        res.send(info);

    });

```

##Send Email Using Template Example

> Email templates can be used to send preformatted HTML emails.
>
> Email templates are saved in /views/emails

```javascript

    //include email module
    const email = require("./modules/email");

    //build object with email information
    let emailOptions = {
            from: {
                name:'IBI Admin',
                address:'admin@ibidata.com'
            },
            to: 'msteiner@ibidata.com',
            subject: 'This is a test',
            template: 'emails/[emailname]', // no extension
            layout: false, // not needed
            context:{
                value1:'string',
                value2:'string'
            },
            log: true // indicates that this email should be saved in the database
        }


    //send email
    email.send(emailOptions,function(err, info){
        if(err) return next(err);

        res.send(info);

    });

```

##Send Email Using NOS

> Email templates can be used to send preformatted HTML emails.
>

```javascript
    //include email module
    const nos = require("./modules/nos");

    //build object with email information
    let emailOptions = {
        from: {
            name:'IBI Admin',
            address:'admin@ibidata.com'
        },
        to: 'msteiner@ibidata.com',
        subject: 'This is a test',
        html: '<h1>Hello World</h1>',
        log: true //indicates that this email should be saved in the database
    }

    //send email
    nos.email.send(emailOptions, function(err, info){
        if(err) return next(err);

        res.send(info);

    });
```



##Validate Email address using BriteVerify

> BriteVerify can be used to check if an email address is real or not.
>
> Note that running this function does cost IBI Data money each time.

```javascript
    //include email module
    const nos = require("./modules/nos");

    //send email
    nos.email.verify('msteiner@ibidata.com', function(err, valid){
        if(err) return next(err);

        //valid is a boolean
        res.send(valid);

    });
```


##Add an Email address to IBI Data's

> This will add an email to IBI Data's opt in table
>


```javascript
    //include email module
    let const = require("./modules/nos");


    let optinOptions = {
        email: 'msteiner@ibidata.com',
        contact: 'Matt Steiner',
        customerno: 'TEST0001',     //optional
        projName: 'General Opt In'  //optional
    }

    //send email
    nos.email.optin('msteiner@ibidata.com', function(err, enrolled){
        if(err) return next(err);

        //enrolled is a boolean, true if opted in
        res.send(enrolled);

    });
```
