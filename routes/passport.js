'use strict';

const config = require('../config');

module.exports = (app, passport) => {

    //=======================================
    // LOGIN PAGE
    //=======================================
    app.get(config.site.url + '/login', (req, res, next) => {
        res.render('login', {
            layout: false,
            message: req.flash('loginMessage')
        });
    });

    //=======================================
    // LOGIN POST PAGE
    //=======================================
    app.post(config.site.url + '/login', (req, res, next) => {
        passport.authenticate('local-login', (err, user, info) => {
            if (err) return next(err);
            if (!user) return res.redirect('./login');
            req.logIn(user, err => {
                if (err) return next(err);
                let destination = req.session.originalURL ? req.session.originalURL : './';
                delete req.session.originalURL;
                return res.redirect(destination);
            });
        })(req, res, next);
    });

    //=======================================
    // LOGOUT POST PAGE
    //=======================================
    app.get(config.site.url + '/logout', (req, res, next) => {
        req.logout();
        res.redirect('./');
    });


};