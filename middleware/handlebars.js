'use strict';

const moment = require('moment');

module.exports = app => {

    const exphbs = require('express-handlebars');

    let handleBarHelpers = exphbs.create({
        defaultLayout: 'main',
        extname: '.hbs',
        helpers: {
            // This is a helper to display decimals for the Costs.
            makeDecimal: (number) => {
                if (number) {
                    return number.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } else {
                    return '0.00';
                }
            },
            // Same as makeDecimal above, but also returns a dollar sign
            dollarAmt: (amount) => {
                if (amount) {
                    return '$' + amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } else {
                    return '';
                }
            },
            // This is a helper to Compare return values -- This is mainly used for internal switches.
            compare: (lvalue, operator, rvalue, options) => {
                let operators, result;
                if (arguments.length < 3) throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
                if (options === undefined) {
                    options = rvalue;
                    rvalue = operator;
                    operator = "===";
                }
                operators = {
                    '==': (l, r) => l == r,
                    '===': (l, r) => l === r,
                    '!=': (l, r) => l != r,
                    '!==': (l, r) => l !== r,
                    '<': (l, r) => l < r,
                    '>': (l, r) => l > r,
                    '<=': (l, r) => l <= r,
                    '>=': (l, r) => l >= r,
                    'typeof': (l, r) => typeof l == r
                };
                if (!operators[operator]) throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
                result = operators[operator](lvalue, rvalue);
                if (result) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            },
            // This helper does math
            math: (lvalue, operator, rvalue, options) => {

                lvalue = parseFloat(lvalue);
                rvalue = parseFloat(rvalue);

                return {
                    '+': lvalue + rvalue,
                    '-': lvalue - rvalue,
                    '*': lvalue * rvalue,
                    '/': lvalue / rvalue,
                    '%': lvalue % rvalue
                }[operator];
            },
            // Format a date/time string without moment (MM/DD/YYYY, hh:mm:ss A). - should return the correct time zone
            localeDateTime: (date) => {
                return new Date(date).toLocaleString();
            },
            // Format a date without moment ('MM/DD/YYYY') - should return the correct time zone
            localeDate: (date) => {
                return new Date(date).toLocaleDateString();
            },
            // Format the time without moment - should return the correct time zone
            localeTime: (date) => {
                return new Date(date).toLocaleTimeString();
            },
            dateTimeFormat: (context, block) => {
                if (moment) {
                    return moment(context).utcOffset('06').format("MMM DD, YYYY hh:mm A"); //had to remove Date(context)
                } else {
                    return context;   //  moment plugin not available. return data as is.
                }
            },
            dateFormat: (context, block) => {
                if (moment) {
                    return moment(context).utcOffset('+06').format('YYYY-MM-DD');
                } else {
                    return context; // Return Without Moment if it's not available.
                }
            },
            // used to show/hide pluralization
            plural: (obj, e) => {
                if (obj.length === 1) {
                    return '';
                } else return e;
            },
            // Return an array index
            index: (arr, i) => {
                return arr[i];
            }
        }
    });

    return handleBarHelpers;

};