(function () {

    // JQUERY
    global.jQuery = require('jquery');
    global.$ = global.jQuery;

    // GLOBAL PLUGINS ====================

    // RESPOND.JS
    require('./plugins/respond');

    // SMOOTH SCROLLING
    require('./plugins/smooth-scrolling');

    // NOS-FORMS
    // require('nos-forms-jquery');

    require('bootstrap/js/transition');
    require('bootstrap/js/dropdown');
    require('bootstrap/js/collapse');

    // GLOBAL METHODS
    global.main = {

        // READ QUERY STRINGS
        querystring: (name) => {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            let regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        name: 'Node Starter',

        myFunction: () => {
            return console.log(this.name);
        }

    };

})();
