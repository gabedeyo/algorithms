'use strict';

const config = require('./config');
const express = require('express');

//============================================
// MIDDLEWARE
//============================================
const app = require('./middleware')(express(), express);
//const passport = require('./middleware/passport')(app);
app.use(require('./middleware/locals'));


//============================================
// ROUTES - GET PAGE TO SEND
//============================================
//require('./routes/passport')(app, passport);
//require('./routes')(app);

app.get(`${config.site.url}/`, (req, res, next) => {
    res.render('index');
})

app.get(`${config.site.url}/breadth-first`, (req, res, next) => {
    res.render('breadth-first');
});

app.get(`${config.site.url}/depth-first`, (req, res, next) => {
    res.render('depth-first');
});

app.get(`${config.site.url}/binary-search`, (req, res, next) => {
    res.render('binary-search');
});

app.get(`${config.site.url}/merge-sort`, (req, res, next) => {
    res.render('merge-sort');
});

app.get(`${config.site.url}/quick-sort`, (req, res, next) => {
    res.render('quick-sort');
});

app.get(`${config.site.url}/tree`, (req, res, next) => {
    res.render('tree');
});

app.get(`${config.site.url}/cloth`, (req, res, next) => {
    res.render('cloth');
});

app.get(`${config.site.url}/testing`, (req, res, next) => {
    res.render('testing');
});

app.get(`${config.site.url}/hash-table`, (req, res, next) => {
    res.render('hash-table');
})


//============================================
// ERROR HANDLING - KEEP AT BOTTOM
//============================================
require('./middleware/errors')(app);

//user IIS port. Can be run as standalone app as well
const listener = app.listen(config.web.port);
console.log('Listening on port %s', listener.address().port);
