'use strict';

const config = require('../../config');
const email = require('../../modules/email');

//SERVER ERROR
module.exports = (err, req, res, next) => {

    res.status(err.status || 500);
    if(err.status === 404) return next();

    //IF THIS IS BEING RUN IN A PRODUCTION ENVIRONMENT, SEND EMAIL TO WEB PROGRAMMERS WITH ERROR, BUT ONLY IF IT IS A SERVER ERROR
    if (!config.debug.testmode) {

        //SHOW USER A 500 ERROR
        res.render('errors/500');

        // REGEX FOR FINDING PASS KEY
        const passTest = /^(.*?[pP][aA][sS][sS][^$]*)$/;

        //LOOP THROUGH BODY KEYS AND CLEAR PASSWORD VALUES
        if(req.body){
            for(let key in req.body){
                if(passTest.test(key)) req.body[key] = "*****";
            }
        }

        //NOW SEND AN EMAIL TO WEB DEVELOPERS
        email.send({
            from: {
                name: config.site.name,
                address: 'weberrors@ibidata.com'
            },
            to: 'weberrors@ibidata.com',
            subject: '[weberrors] ' + config.site.name + ': an error occured',
            template: 'emails/error',
            context: {
                url: req.protocol + '://' + req.headers.host + req.url,
                client: config.client.code,
                err: JSON.stringify(err),
                method: req.method,
                body: JSON.stringify(req.body),
                json: JSON.stringify(req.json),
                session: JSON.stringify(req.session),
                useragent: JSON.stringify(req.headers['user-agent'])
            }
        });

    } else {
        //IF DEV, JUST SHOW THE ERROR IN THE BROWSER
        console.log(err);
        res.send(JSON.stringify(err));
        res.end();
    }
};
