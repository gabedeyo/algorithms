'use strict';

const config = require('../config'),
    fs = require('fs');

const aws = require('aws-sdk');
aws.config.region = config.aws.region;
aws.config.update(config.aws.credentials);

//S3 Methods ================================================
aws.s3 = {};
aws.s3.uploadFile = (params, cb) => {


    fs.readFile(params.filepath, (err, data) => {

        //PROBLEM READING FILE, RETURN ERROR
        if (err && typeof cb === 'function') {
            cb(err, null);
        }


        let base64data = new Buffer(data, 'binary');

        let s3 = new aws.S3();

        s3.putObject({
            Bucket: params.bucket,
            Key: params.s3path,
            Body: base64data,
            ContentType: 'application/octet-stream',
            ACL: 'public-read'
        }, resp => {
            if (typeof cb === 'function') {
                cb(resp, arguments[1]);
            }
        })

    })


}

aws.s3.downloadFile = (params, cb) => {

    let s3 = new aws.S3();
    let file = fs.createWriteStream(params.filepath);

    let getobject = s3.getObject({
        Bucket: params.bucket,
        Key: params.s3path
    });

    getobject.createReadStream().pipe(file);

    getobject.on('success', response => {
        if (typeof cb === 'function') {
            cb(null, response.httpResponse.headers);
        }
    })

    getobject.on('error', response => {
        if (typeof cb === 'function') {
            cb(response, null);
        }
    });
};




aws.s3.listBuckets = (cb) => {

    let s3 = new aws.S3();
    s3.listBuckets((err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });
};



aws.s3.listObjects = (params, cb) => {


    let s3 = new aws.S3();


    s3.listObjects({
        Bucket: params.bucket,
        Marker: params.s3dir
    }, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });

};




//RDS Methods ================================================
aws.rds = {}
aws.rds.createDBSnapshot = (params, cb) => {
    let rds = new aws.RDS();
    rds.createDBSnapshot({
        DBInstanceIdentifier: params.dbName,
        DBSnapshotIdentifier: params.snapshotName
    }, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });
};



//SNS Methods ================================================
aws.sns = {}

aws.sns.publishSMS = (params, cb) => {

    aws.config.region = 'us-east-1'; //us-east-1 is the only region that supports SMS
    let sns = new aws.SNS();

    let snsparams = {
        Message: params.message,
        TopicArn: params.topicArn
    };


    sns.publish(snsparams, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });


};


aws.sns.subscribeSMS = (params, cb) => {

    aws.config.region = 'us-east-1'; //us-east-1 is the only region that supports SMS
    let sns = new aws.SNS();

    let snsparams = {
        Protocol: 'sms',
        TopicArn: params.topicArn,
        Endpoint: params.phoneNumber
    };

    sns.subscribe(snsparams, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });

};


aws.sns.unsubscribeSMS = (params, cb) => {

    aws.config.region = 'us-east-1'; //us-east-1 is the only region that supports SMS
    let sns = new aws.SNS();

    let snsparams = {
        SubscriptionArn: params.SubscriptionArn
    };

    sns.unsubscribe(snsparams, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });
};



aws.sns.listSubscriptionsByTopicSMS = (params, cb) => {

    aws.config.region = 'us-east-1'; //us-east-1 is the only region that supports SMS
    let sns = new aws.SNS();

    let snsparams = {
        TopicArn: params.topicArn
    };

    sns.listSubscriptionsByTopic(snsparams, (err, data) => {
        if (typeof cb === 'function') {
            cb(err, data);
        }
    });

};


module.exports = aws;