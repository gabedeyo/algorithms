(function() {

    var canvas = document.getElementById('testCanvas');
    var ctx = drawInitial(canvas);

    // Create and fill sorted array
    var array = [];
    for(let i = 0; i < 100; i++ ){
        array[i] = i+1;
    }

    // Create hidden value between 0 and 100
    var hiddenValue = Math.floor(Math.random() * (array[array.length-1] - array[0] + 1) + array[0]);

    // Store Binary Search result in index and print index on canvas
    var index = BinarySearch(array, hiddenValue, ctx);
    var printString = "Secret value " + hiddenValue + " found at array index " + index;
    drawHeading(ctx, printString, -200);

})();

function drawInitial(canvas){

    let windowWidth = $(document).width();
    let windowHeight = $(document).height();

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = windowWidth / 3;
    ctx.canvas.height = windowHeight / 2;

    return ctx;
}

function drawHeading(ctx, printString, offset){
    ctx.font="25px sans-serif";
    ctx.textAlign="center";

    ctx.fillText(printString, ctx.canvas.width/2, ctx.canvas.height/2-offset);
}

function BinarySearch(array, value, ctx){
    let min = 0;
    let max = array.length - 1;
    let count = 0;
    let guess;

    let offset = 200;

    while(min <= max){
        guess = Math.floor((max+min)/2);

        drawHeading(ctx, "Min: " + min + " Max: " + max + " Guess: " + guess, offset);
        offset -= 50;

        count++;

        if(array[guess] == value){
            return guess;
        }else if(array[guess] < value){
            min = guess + 1;
        }else{
            max = guess - 1;
        }
    }
    return -1;
}
