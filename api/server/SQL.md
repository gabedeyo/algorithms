#SQL

> sql uses node-mssql. More information can be found on their [github](https://github.com/patriksimek/node-mssql).
>
> Database settings can be changed in config.js

####Using a stored procedures

```javascript
    //include database module
    const sql = require('mssql');

    //connect to database and
    sql.connect(config.sql, function(err) {
        if(err) return next(err);

        let request = new sql.Request();

        //pass in stored procedure variables here
        request.input('@value2', var1)
        request.input('@value2', var2)

        //execude stored procedure
        request.execute('proc_sp_name', function(err, recordset, returnValue) {
            if(err) return next(err);

            res.send(recordset)

        });
    });
```

####Using SQL Query

```javascript
    //include database module
    const sql = require('mssql');

    //connect to database and
    sql.connect(config.sql, function(err) {
        if(err) return next(err);

        let request = new sql.Request();

        //execute query
        request.query('SELECT * FROM table', function(err, recordset, returnValue) {
            if(err) return next(err);

            res.send(recordset);

        });
    });
```


###NOS Shorthand
```javascript
    const nos = require('./modules/nos');

    //SQL Query with callbacks
    nos.sql.query('SELECT * FROM logins', function(err, recordset, returnValue) {
        if(err) return next(err);

        res.send(recordset);

    });

    //SQL Query with promises (uses Bluebird)
    nos.sql.queryAsync('SELECT * FROM logins')

        .then(function (recordset) {

            res.send(recordset);

        })

        .catch(function (err) {

            //Handle error

        });



    //Stored Procedure
    let vars = {
        var1:'value',
        var2:'value'
    }


    //Stored Procedure with callbacks
    nos.sql.storedProcedure('sproc_name', vars, function(err, recordset, returnValue) {
        if(err) return next(err);

        res.send(recordset);

    });


    //Stored Procedure with promises (uses Bluebird)
    nos.sql.storedProcedureAsync('sproc_name', vars)

        .then(function (recordset) {

            res.send(recordset);

        })

        .catch(function (err) {

            //Handle error

        });


```
