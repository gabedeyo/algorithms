// Utility Scripts


// ** Redirect to Https if not already there!  ** /
if (window.location.protocol != "https:")
    window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);



// ** Getting QueryString ** /
// One that starts with Some of the Solutions posted here are inefficient.
// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
let urlParams;
(window.onpopstate = function() {
    let match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function(s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
})();

// Grabbing the QueryString parameters that will be used.
const urlvar = urlParams['queryStringVar'];





const utilities = {

	// ** Required Fields Check ** //
	// Pass in the Panel that needs to be checked - no need to include ID
	// This does not require jQuery
	isFormValid: function(checkingWhat) {
	    // Resets to make everything clear before the loop.
	    let valid = true;
	    document.querySelector('#' + checkingWhat + ' .errorMsg').style.display = 'none';

	    // Anything with the req class on it will be checked.
	    checkingReqElements = document.querySelectorAll('#' + checkingWhat + ' .req');

	    Array.prototype.forEach.call(function(checkingReqElements, i) {

	        this.classList.remove('error');

	        if (this.value != null) {
	            if (this.value.length || this.value.trim() == "") {
	                valid = false;
	                this.classList.add('error');
	                this.focus();
	                errorShow(checkingWhat, 'Please fill out required fields');
	            }
	        } else {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please fill out required fields');
	        }

	    });

	    if (valid == false) {
	        $('.error').first().focus();
	        return valid;
	    }

	    // Doing a check for Zip Code.
	    checkingZipElements = document.querySelectorAll('#' + checkingWhat + ' .zip');

	    Array.prototype.forEach.call(function(checkingZipElements, i) {

	        this.classList.remove('error');

	        zipPattern = /^\d{5}([-]\d{4})?$/; // 5 to 10 digit Zip Code (ex 55555 or 55555-5555)
	        if (this.value.match(zipPattern) == null) {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please provide a valid Zip Code');
	        }

	    });

	    // Doing a check for Phone numbers.
	    checkingPhoneElements = document.querySelectorAll('#' + checkingWhat + ' .phone');

	    Array.prototype.forEach.call(function(checkingPhoneElements, i) {

	        this.classList.remove('error');

	        phonePattern = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s-]\d{3}[\s-]\d{4}$/; // 10 digit Phone Regex (ex 111-111-1111)
	        if (this.value.match(phonePattern) == null) {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please provide a valid Phone Number');
	        }

	    });

	    // Doing a check for Valid Date numbers. -- Will need to Test this one
	    checkingDateElements = document.querySelectorAll('#' + checkingWhat + ' .date');

	    Array.prototype.forEach.call(function(checkingDateElements, i) {

	        this.classList.remove('error');

	        datePattern = /^\d{1,2}(-|\/)\d{1,2}(-|\/)\d{2,4}$/; // Date Regex
	        if (this.value.match(datePattern) == null) {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please provide a valid Date');
	        }

	    });

	    // Doing a check for Valid time. -- Will need to Test this one
	    checkingTimeElements = document.querySelectorAll('#' + checkingWhat + ' .time');

	    Array.prototype.forEach.call(function(checkingTimeElements, i) {

	        this.classList.remove('error');

	        timePattern = /^(1[0-2]|0[1-9]):[0-5][0-9]\040(AM|am|PM|pm)$/; // Time Regex
	        if (this.value.match(timePattern) == null) {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please provide a valid Time');
	        }

	    });

	    // Doing a check for Email.
	    checkingEmailElements = document.querySelectorAll('#' + checkingWhat + ' .time');

	    Array.prototype.forEach.call(function(checkingEmailElements, i) {

	        this.classList.remove('error');

	        emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // Email address (ex xxx@xxx.com)
	        if (this.value.match(emailPattern) == null) {
	            valid = false;
	            this.classList.add('error');
	            this.focus();
	            errorShow(checkingWhat, 'Please provide a valid Email Address');
	        }
	    });

	    if (valid == false) {
	        return valid;
	    }

	    return valid;
	},

	// Displays an Error and message for it.
	errorShow: function(panel, message) {
	    document.querySelector('#' + panel + '  .errorMsg').innerHTML = message;
	    document.querySelector('#' + panel + '  .errorMsg').style.display = 'block';
	},

	// Clear the Errors so it isn't annoying if the user is actually trying to fix things.
	clearErrors: function(panel) {
	    clearingElements = document.querySelector('#' + panel + ' .req');
	    Array.prototype.forEach.call(function(checkingElements, i){
	        this.classList.remove('error');
	    });
	    document.querySelector('#' + panel + ' .errorMsg').style.display = 'none';
	},


	// ** Password Complexity Check ** /
	// Variable password  = Password to validate
	// Variable minLength = Minimum number of password characters.
	// Variable numUpper = Minimum number of uppercase characters.
	// Variable numLower = Minimum number of lowercase characters.
	// Variable numNumber = Minimum number of numeric characters.
	// Variable numSpecial = Minimum number of special characters.
	// Variable numNeeded = How many of the following do you need.
	// Will Return True if it is complex enough.
	validatePassword: function(password, minLength, numUpper, numLower, numNumber, numSpecial, numNeeded) {
		const upperPattern = /[A-Z]/;
		const lowerPattern = /[a-z]/;
		const numberPattern = /[0-9]/;
		const specialPattern = /[$&+,:;=?@#|'<>.-^*()%!]/;

		if (password.length < minLength) {
			return false;
		}

		let numCheckNeeded = 0;

		if (password.match(upperPattern) != null && password.match(upperPattern).length >= numUpper) {
			numCheckNeeded++;
			console.log('passed upper')
			console.log(numCheckNeeded)
		}

		if (password.match(lowerPattern) != null && password.match(lowerPattern).length >= numLower) {
			numCheckNeeded++;
			console.log('passed lower')
			console.log(numCheckNeeded)
		}

		if (password.match(upperPattern) != null && password.match(upperPattern).length >= numNumber) {
			numCheckNeeded++;
			console.log('passed Number')
			console.log(numCheckNeeded)
		}

		if (password.match(specialPattern) != null && password.match(specialPattern).length >= numSpecial) {
			numCheckNeeded++;
			console.log('passed special')
			console.log(numCheckNeeded)
		}

		if (numCheckNeeded < numNeeded) {
			return false;
		}

		return true;
	},


	// JavaScript Only Solution to the Top
	// function runScroll() {
	//   scrollTo(document.body, 0, 600);
	// }
	// var scrollme;
	// scrollme = document.querySelector("#scrollme");
	// scrollme.addEventListener("click",runScroll,false)

	scrollTo: function (element, to, duration) {
	    if (duration < 0) return;
	    const difference = to - element.scrollTop;
	    const perTick = difference / duration * 10;

	    setTimeout(function() {
	        element.scrollTop = element.scrollTop + perTick;
	        if (element.scrollTop === to) return;
	        scrollTo(element, to, duration - 10);
	    }, 10);
	},


	// ** Formatting File Sizes ** /
	//Converts bytes into a more human readable format.
	//Bytes to be converted into a file size
	//Number with proper file size suffix.
	formatFileSize :function(size, unitcss) {
		const suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
		let s = 0

		while (size >= 1024) {
			s++
			size /= 1024
		}

		if (unitcss == true) {
			return Math.round(size * 100/ 100) + '<span class="unit">' + suffixes[s] + '</span>';
		} else {
			return Math.round(size * 100/ 100) + ' ' + suffixes[s];
		}
	},

	// Encryption String with Node
	// Documentation: https://nodejs.org/docs/latest/api/crypto.html#crypto_crypto_createcipher_algorithm_password
	// Example pulled from: https://gist.github.com/csanz/1181250
	// crypto.createCipheriv(algorithm, key, iv)
	// cipher.update(data[, input_encoding][, output_encoding])
	// Example Use: var hw = encrypt("hello world")
	encrypt: function(text) {
	  let cipher = crypto.createCipher('aes-256-cbc','goIbi')
	  let crypted = cipher.update(text,'utf8','hex')
	  crypted += cipher.final('hex');
	  return crypted;
	},

	// Decryption String with Node
	// Documentation: https://nodejs.org/docs/latest/api/crypto.html#crypto_crypto_createdecipher_algorithm_password
	// crypto.createDecipher(algorithm, password)
	// decipher.update(data[, input_encoding][, output_encoding])
	// Example Use: decrypt(hw)
	decrypt: function(text) {
	  let decipher = crypto.createDecipher('aes-256-cbc','goIbi')
	  let dec = decipher.update(text,'hex','utf8')
	  dec += decipher.final('utf8');
	  return dec;
	}
}






















// ** This will be Removed once the other testing is done, used as reference only ** /



// ** Required Field Check  ** /
// Loop through for all the required fields being filled out.
// jQuery is Required for this Version
const isFormValidjQuery = function(checkingWhat) {
    // Resets to make everything clear before the loop.
    let valid = true;
    $('#' + checkingWhat + ' .errorMsg').css('display', 'none');
    // Anything with the req class on it will be checked.
    $('#' + checkingWhat + ' .req').each(function() {
        $(this).removeClass('error');
        if ($(this).val() != null) {
            if (!$(this).val().length || $(this).val().trim() == "") {
                valid = false;
                $(this).addClass('error');
                $(this).focus();
                errorShow(checkingWhat, 'Please fill out required fields');
            }
        } else {
            valid = false;
            $(this).addClass('error');
            $(this).focus();
            errorShow(checkingWhat, 'Please fill out required fields');
        }
    });

    if (valid == false) {
        $('.error').first().focus();
        return valid;
    }

    // Doing a check for Zip Code.
    $('#' + checkingWhat + ' .zip').each(function() {
        $(this).removeClass('error');

        zipPattern = /^\d{5}([-]\d{4})?$/; // 5 to 10 digit Zip Code (ex 55555 or 55555-5555)
        if ($(this).val().match(zipPattern) == null) {
            valid = false;
            $(this).addClass('error');
            $(this).focus();
            errorShow(checkingWhat, 'Please provide a valid Zip Code');
        }
    });

    // Doing a check for Phone numbers.
    $('#' + checkingWhat + ' .phone').each(function() {
        $(this).removeClass('error');
        phonePattern = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s-]\d{3}[\s-]\d{4}$/; // 10 digit Phone Regex (ex 111-111-1111)
        if ($(this).val() != null && $(this).val().length && $(this).val().trim() != "") {
            if ($(this).val().match(phonePattern) == null) {
                valid = false;
                $(this).addClass('error');
                $(this).focus();
                errorShow(checkingWhat, 'Please provide a valid Phone Number');
            }
        }
    });

    // Doing a check for Valid Date numbers. -- Will need to Test this one
    $('#' + checkingWhat + ' .date').each(function() {
        $(this).removeClass('error');
        datePattern = /^\d{1,2}(-|\/)\d{1,2}(-|\/)\d{2,4}$/; // Date Regex
        if ($(this).val() != null && $(this).val().length && $(this).val().trim() != "") {
            if ($(this).val().match(datePattern) == null) {
                valid = false;
                $(this).addClass('error');
                $(this).focus();
                errorShow(checkingWhat, 'Please provide a valid Date');
            }
        }
    });

    // Doing a check for Valid time. -- Will need to Test this one
    $('#' + checkingWhat + ' .date').each(function() {
        $(this).removeClass('error');
        timePattern = /^(1[0-2]|0[1-9]):[0-5][0-9]\040(AM|am|PM|pm)$/; // Time Regex
        if ($(this).val() != null && $(this).val().length && $(this).val().trim() != "") {
            if ($(this).val().match(timePattern) == null) {
                valid = false;
                $(this).addClass('error');
                $(this).focus();
                errorShow(checkingWhat, 'Please provide a valid Time');
            }
        }
    });

    // Doing a check for Email.
    $('#' + checkingWhat + ' .email').each(function() {
        $(this).removeClass('error');
        emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // Email address (ex xxx@xxx.com)
        if ($(this).val().match(emailPattern) == null) {
            valid = false;
            $(this).addClass('error');
            $(this).focus();
            errorShow(checkingWhat, 'Please provide a valid Email Address');
        }
    });

    if (valid == false) {
        $('.error').first().focus();
        return valid;
    }

    $('.error').first().focus();
    return valid;

};


// Displays an Error and message for it.
const errorShow = function(panel, message) {
    $('#' + panel + ' .errorMsg').text(message);
    $('#' + panel + ' .errorMsg').css('display', 'block');
}


// Clear the Errors so it isn't annoying if the user is actually trying to fix things.
const clearErrors = function(panel) {
    $('#' + panel + ' .req').each(function() {
        $(this).removeClass('error');
    });
    $('#' + panel + ' .errorMsg').css('display', 'none');
}

// This Script Will need to be used with jQuery and in the document.ready() function
//** Blurring Phone with 10 Chars will format automatically. ** /
// $('#form').on('blur', '.phone', function() {
//     var phone = $(this).val();
//     phone = phone.replace(/\D/g, '');
//     if (phone.length === 10) {
//         $(this).val(phone.slice(0, 3) + ' ' + phone.slice(3, 6) + '-' + phone.slice(6, 10));
//     }
// })



// ** Nice Scrolling down the page  ** /
// Pass in the actual target example: $('#prevGuarantees') -- this would be an id'd anchor <a id="prevGuarantees"></a>
const scrollToSomePlace = function(somePlace) {
    let target = somePlace
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
        $('html,body').animate({
            scrollTop: target.offset().top
        }, 800);
        return false;
    }
}
