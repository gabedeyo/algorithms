'use strict';

const multer = require('multer');
const config = require('../config');
const util = require('./util');

//STORAGE SETTINGS (Disk Storage)
let storage = multer.diskStorage({

    //SET UPLOAD DESTINATION
    destination: (req, file, cb) => cb(null, config.upload.destination),

    //SET NEW FILENAME
    filename: (req, file, cb) => cb(null, file.fieldname + '-' + Date.now())

});

let upload = multer({

    //STORAGE SETTINGS
    storage: storage,

    //FILE LIMITS
    limits: {
        fileSize: config.upload.maxFileSize * 1000000,
        files: config.upload.maxFiles
    },

    //FILTER OUT UPLOADED FILES
    fileFilter: (req, file, cb) => {

        //FILTER ONLY IF config.upload.accept EXISTS
        if (config.upload.accept) {

            if (config.upload.accept.indexOf(util.fileExt(file.originalname).toLowerCase()) > -1) {
                cb(null, true);
            } else {
                //If you want to trigger an error on failed upload, do that here
                cb(null, false);
            }

        } else {
            //ACCEPT FILE IF NO RESTRICTIONS
            cb(null, true);
        }

    }
});

module.exports = upload;