#MD5

>MD5 Can be used to hash a string or check the integrity of a file
>
>MD5 Uses the node-md5. More information can be found on their [github](https://github.com/pvorb/node-md5)

####Hashing String with MD5

```javascript

    const md5 = require('md5');
    md5('message');

    //output: '78e731027d8fd50ed642340b7c9a63b3'

```

####Get File MD5 Checksum

```javascript

    const fs = require('fs');
    const md5 = require('md5');

    fs.readFile('example.txt', function(err, buf) {
        if(err) return next(err);

        console.log(md5(buf));
    });

```
