'use strict';

const config = require('../config');

module.exports = (app, express) => {




    //============================================
    //REMOVE POWERED BY HEADER
    //============================================
    app.disable('x-powered-by');


    //============================================
    // CREATE RANDOM STRING FOR NONCE SCRIPTING
    //============================================
    function nonceID(){
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(let i=0; i < 10; i++)
            text += possible.charAt(~~(Math.random() * possible.length));

        return text;
    }

    //============================================
    // SET HEADER WITH CONTENT SECURITY POLICY
    //============================================
    app.use(function(req, res, next){
        if(!config.debug.testmode){
            res.locals.nonce = nonceID();
            res.setHeader('Content-Security-Policy', "style-src 'self' 'unsafe-inline'; script-src 'nonce-"+res.locals.nonce+"'; object-src 'self'");
        }
        
        next();
    })


    //============================================
    // SET HANDLEBARS AS THE RENDER ENGINE
    //============================================
    app.engine('hbs', require('./handlebars')(app).engine); //exphbs()
    app.set('view engine', 'hbs');




    //============================================
    // ADD BODY PARSER TO HANDLE FORM DATA
    //============================================
    const bodyParser = require("body-parser");
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());




    //============================================
    // ENABLE COMPRESSION
    //============================================
    const compression = require('compression');
    app.use(compression());




    // ============================================
    // ADD IS INTERNAL TO REQ
    // ============================================
    app.use((req, res, next) => {
        req.isInternal = false;

        let ip = req.ip;
        if(req.headers['x-forwarded-for']) ip = req.headers['x-forwarded-for'].split(',')[0].split(':')[0];
        if(ip) req.isInternal = ['192.168.2','184.105.1','63.164.68','12.30.67.','127.0.0.1','::1', '::ffff:12'].indexOf(ip.substring(0,9)) !== -1;

        next();
    });




    ////============================================
    //// SETUP SESSION HANDLING
    ////============================================
    // const session = require('express-session');
    // const MSSQLStore = require('connect-mssql')(session);
    // let settings = {
    //     store: new MSSQLStore(config.sql),
    //     secret: 'Welcome_to_IBI_Data',
    //     resave: false,
    //     saveUninitialized: false
    // };

    // //SECURE COOKIES IF IN PRODUCTION
    // if(!config.debug.testmode){
    //     app.set('trust proxy', 1);
    //     settings.cookie = { secure: true };
    // }

    // app.use(session(settings));

    // THIS TABLE NEEDS TO EXIST TO HANDLE SESSIONS WITH MSSQLSTORE
    /*
        CREATE TABLE [dbo].[sessions](
            [sid] [varchar](255) NOT NULL PRIMARY KEY,
            [session] [varchar](max) NOT NULL,
            [expires] [datetime] NOT NULL
        )
        GRANT INSERT ON [dbo].[sessions] TO [WebUser]
        GRANT SELECT ON [dbo].[sessions] TO [WebUser]
        GRANT UPDATE ON [dbo].[sessions] TO [WebUser]
    */




    //============================================
    // LIVERELOAD --ONLY IN DEVELOPMENT
    //============================================
    if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
        app.use(require('connect-livereload')({
            //IGNORE ALL DOWNLOADABLE FILE EXTENSIONS (OTHERWISE WILL TRY TO INJECT LIVERELOAD INTO BROWSER DOWNLOADED FILES)
            ignore: ['.csv', '.xls', '.xlsx', '.pdf', '.jpg', '.jpeg', '.png', '.gif', '.svg', '.doc', '.docx', '.txt']
        }));
    }




    //============================================
    // FRONT END DEPENDENCIES
    //============================================
    app.use(config.site.url + '/scripts*', (req, res, next) => {
        //IF YOU NEED TO RETRIEVE OTHER FILE EXTENSIONS, ADD THEM TO THE ARRAY BELOW
        if (['js', 'otf', 'eot', 'svg', 'woff', 'woff2', 'ttf'].indexOf(req.originalUrl.split('?')[0].split('.').pop()) > -1) {
            next();
        } else next({status:404, message:'resource not found'});
    });

    app.use(config.site.url + '/scripts', express.static(__dirname + '/../node_modules'));




    //============================================
    // HANDLE STATIC CONTENT
    //============================================
    app.use(config.site.url, express.static(__dirname + '/../public'));

    //IF RESOURCE IS UNDER  THE ASSETS DIRECTORY AND CAN'T BE FOUND, NO REASON TO RUN THROUGH OTHER ROUTES.
    app.all(config.site.url + '/assets*', (req, res, next) => {
        next({status:404, message:'resource not found'});
    });







    return app;
};
