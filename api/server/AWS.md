#Amazon Web Services
>This uses the AWS-SDK library. Their github can be found [here](https://github.com/aws/aws-sdk-js)
>
> [Full Documentation of the SDK](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/top-level-namespace.html)

#S3

##Upload File to S3 Bucket

```javascript
    const nos = require('./modules/nos');

    let params = {
        filepath: './text.txt',
        bucket: 'ibidata',
        s3path:'test/text.txt'
    }

    nos.aws.s3.uploadFile(params, function(err, data){
        if(err) return next(err);

        //upload success

    });
```

##Download File from S3 Bucket

```javascript
    const nos = require('./modules/nos');

    let params = {
        filepath: './text.txt',
        bucket: 'ibidata',
        s3path:'test/text.txt'
    }

    nos.aws.s3.downloadFile(params, function(err, data){
        if(err) return next(err);

        //download success

    });
```
##List S3 Buckets

```javascript
    const nos = require('./modules/nos');

    nos.aws.s3.listBuckets(function(err, data){
		if(err) return next(err);

		res.end(JSON.stringify(data));
	})
```    

##List S3 Objects Within Bucket

```javascript
	let params = {
		bucket: 'ibidata',
		s3dir: 'website/assets/'
	};


    nos.aws.s3.listObjects(params, function(err, data){
		console.log(data);
		res.end(JSON.stringify(data));
	})
```    

#RDS

##Create DB Snapshot
```javascript
    const nos = require('./modules/nos');

    let params = {
        dbName: 'ibi-aws',
        snapshotName: 'ibi-aws-2015-10-30'
    }
    nos.aws.rds.createDBSnapshot(params,function(err, data){
        if(err) return next(err);

        //success

    });
```


#SNS

>Note about SMS: SMS only works in the AWS East region. There are settings within the function that specify to use the East when sending SMS messages. Remember to select the East region when setting up a new topic.

##Subscribe User to SMS Message Topic

```javascript
    let params = {
        topicArn: 'arn:aws:sns:us-east-1:910932988609:IBIData', //This is found when setting up an SNS topic
        phoneNumber: '15070000000'
    }

    nos.aws.sns.subscribeSMS(params, function(err, data) {
        if (err) return next(err);

        //success

    })
```


##Unsubscribe User from SMS Message Topic

```javascript
    let params = {
        SubscriptionArn: 'SubscriptionArn' //This is found when listing an SNS topic
    }

    nos.aws.sns.unsubscribeSMS(params, function(err, data) {
        if (err) return next(err);

        //success

    })
```



##List Users subscribed to SMS Message Topic

```javascript
    let params = {
        topicArn: 'arn:aws:sns:us-east-1:910932988609:IBIData', //This is found when setting up an SNS topic
    }

    nos.aws.sns.listSubscriptionsByTopicSMS(params, function(err, data) {
        if (err) return next(err);

        //success

    })
```



##Send SMS Message to Topic
```javascript
    let params = {
        message: "This is a test",
        topicArn: 'arn:aws:sns:us-east-1:910932988609:IBIData' //This is found when setting up an SNS topic
    }

    nos.aws.sns.publishSMS(params, function(err, data) {
        if (err) return next(err);

        //success

    })
```
