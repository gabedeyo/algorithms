// Place Data Structure Classes

(function(){
    // Get Canvas Elements
    var canvas = document.getElementById('testCanvas');
    var ctx = drawInitial(canvas);

    //SelectionSort();
    InsertionSort();

})();

function InsertionSort(){

    var insert = function(array, rightIndex, value){
        var index;
        for(index = rightIndex; index >= 0 && array[index] > value; index--){
            array[index + 1] = array[index];
        }
        array[index+1] = value;
    }

    var insertionSort = function(array){
        for(var i = 1; i < array.length; i++){
            insert(array, i-1, array[i]);
        }
    }

    var array = [];
    for(let i = 0; i < 5; i++){
        var val = Math.floor(Math.random() * (100 - 0 + 1) + 0);
        array[i] = val;
    }

    console.log(array);
    insertionSort(array);
    console.log(array);
}

function SelectionSort(){
    // Selection Sort Algorithm
    var swap = function(array, firstIndex, secondIndex){
        var temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }

    var indexOfMinimum = function(array, startIndex) {
        var minValue = array[startIndex];
        var minIndex = startIndex;

        for(var i = minIndex + 1; i < array.length; i++) {
            if(array[i] < minValue) {
                minIndex = i;
                minValue = array[i];
            }
        }
        return minIndex;
    };

    var selectionSort = function(array){
        var minIndex;

        for(var i = 0; i < array.length; i++){
            minIndex = indexOfMinimum(array, i);
            swap(array, i, minIndex);
        }
    }

    var array = [];
    for(let i = 0; i < 10; i++){
        var val = Math.floor(Math.random() * (100 - 0 + 1) + 0);
        array[i] = val;
    }

    console.log(array);
    selectionSort(array);
    console.log(array);
}

//Draw Inital Canvas Elements
function drawInitial(canvas){
    let windowWidth = $(document).width();
    let windowHeight = $(document).height();

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = windowWidth / 3;
    ctx.canvas.height = windowHeight / 2;

    return ctx;
}

function drawHeading(ctx, printString, x, y){
    ctx.font="25px sans-serif";
    ctx.textAlign="center";

    ctx.fillText(printString, ctx.canvas.width/2 - x, ctx.canvas.height/2 - y);
}
