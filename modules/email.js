'use strict';

const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const hbs = require('nodemailer-express-handlebars');
const config = require('../config');
const engine = require('../middleware/handlebars')();
const mssql = require('mssql');
const request = require('request');

//SET MAIL SQL CONFIGURATION
const sqlconfig = {
    database: 'Emails',
    user:'Emails',
    password: 'd0n0temailme',
    server: (config.debug.testmode ? 'SQLDEV': 'SQL2005'),
    port: '1203'
}


module.exports = {

    /*
     *    mailOptions {
     *            from: {
     *                name:'Matt Steiner',
     *                address:'msteiner@ibidata.com'
     *            },
     *            to: 'msteiner@ibidata.com',
     *            subject: 'Hello',
     *            html: 'Hello world'
     *        }
     *
     *    Full List of Email Options can be found here:
     *    https://github.com/andris9/Nodemailer#e-mail-message-fields
     */
    send: (mailOptions, callback) => {

        //SETUP EMAIL OPTIONS
        mailOptions = mailOptions || {
            from: {
                name: 'IBI Admin',
                address: 'admin@ibidata.com'
            },
            to: 'msteiner@ibidata.com',
            subject: '',
            html: '',
            log: false
        };

        //IF NO FROM EMAIL IS PROVIDED, JUST USE WHAT IS IN THE CONFIG
        if(!mailOptions.from) mailOptions.from = { name: config.site.emailname, address: config.site.email }

        //IF IS IN TESTMODE, DON'T SEND THE EMAIL OUT
        if (config.debug.testmode) {
            if(!config.tester.email) return console.error('\x1b[31mNo tester email has been defined in config.js');
            mailOptions.to = config.tester.email;
            mailOptions.cc = null;
            mailOptions.bcc = null;
        }

        //SETUP THE EMAIL SERVER CONNECTION
        let transporter = nodemailer.createTransport(smtpTransport(config.smtp));

        //ATTACH THE HANDLEBARS MOD IF TEMPLATE IS PASSED IN
        if (mailOptions.template !== undefined) {

            // IF WE ARE USING HANDLEBARS, MAKE SURE WE AREN'T DEFAULTING TO THE MAIN LAYOUT. IF A LAYOUT IS SPECIFIED, USE IT
            engine.defaultLayout = mailOptions.layout;

            transporter.use('compile', hbs({
                viewEngine: engine,
                extName: '.hbs',
                viewPath: (__dirname + '/../views')
            }));
        }

        //SEND EMAIL
        transporter.sendMail(mailOptions, (err, info) => {
            if(typeof callback === 'function'){
                callback(err, info);
            }
        });


        //LOG EMAIL TO DATABASE IF NEEDED
        if(mailOptions.log || false){

            //SET CONNECTION
            mssql.connect(sqlconfig, err => {

                //IF WE ARE USING A TEMPLATE, PASS THE PATH TO THE FILE AND THE DATA THAT IS BEING USED TO THE DATABASE
                if(mailOptions.template){
                    mailOptions.html = 'Template: "' + mailOptions.template + '.handlebars"';

                    if(mailOptions.context){
                        mailOptions.html += ' | Data:' +  JSON.stringify(mailOptions.context);
                    }
                }


                //MAKE SURE ANY ARRAY IS SPLIT OUT INTO A SINGLE STRING
                let to = mailOptions.to;
                if(Array.isArray(to)) to = to.toString();

                let bcc = mailOptions.bcc;
                if(Array.isArray(bcc)) bcc = bcc.toString();

                let cc = mailOptions.cc;
                if(Array.isArray(cc)) cc = cc.toString();


                //SET STORED PROCEDURE VARIABLES
                let request = new mssql.Request();
                request.input('emailto', to || '');
                request.input('emailbcc', bcc || '');
                request.input('emailcc', cc || '');
                request.input('subject', mailOptions.subject || '');
                request.input('emailfrom', mailOptions.from.address || '');
                request.input('body', mailOptions.html || mailOptions.text || '');
                request.input('client', config.client.code || '');
                request.input('project', config.site.name || '');
                request.input('executingProgram', config.site.url || '');
                request.input('attach', mailOptions.path || '');

                //EXECUTE STORED PROCEDURE
                request.execute('addsent');
            });
        }
    },

    // This will run a verification on an email address to make sure it is real and valid
    // This will charge IBI's BriteVerify account everytime a request is run
    verify: (address, callback) => {

        // Do a quick regex check to see if a valid email format is getting passed back. If not, no need to use briteverify
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(address)) return callback(null, false);


        //TODO: Add a verification to check if email is on a list of approved domains. (ibidata.com, ryangroup.com) If so, just return true.


        // Use briteverify to check if email is valid.
        request({
            url: 'https://bpi.briteverify.com/emails.json',
            method:'GET',
            rejectUnauthorized: false,
            qs: { address, apikey: 'cf5b08a6-976c-4f46-9b86-ac65be82e24b' }
        }, function(err, response, body){

            // if err, return it
            if(err) return callback(err);

            // check to make sure briteverify returned a proper 200 status
            if(response.statusCode !== 200)  return callback(new Error('Briteverify did not return successfully'))

            //convert the body to json
            body = JSON.parse(body);

            // check to make sure a proper status was returned
            if(!body.status) return callback(new Error('Briteverify did not return a valid status'))

            // all is good, return valid or not
            callback(null, (body.status === "valid"));
        })
    },

    /*
     *    optinOptions {
     *        email: 'msteiner@ibidata.com',
     *        contact: 'Matt Steiner'
     *    }
     */
    optin: (optinOptions, callback) =>{

        //SET CONNECTION
        mssql.connect(sqlconfig, err => {

            if(err) return callback(err);

            //SET STORED PROCEDURE VARIABLES
            let request = new mssql.Request();
            request.input('email', optinOptions.email);
            request.input('contact', optinOptions.contact || '');
            request.input('ProjName', optinOptions.projName || 'General Opt In');
            request.input('customerno', optinOptions.customerno || '');
            request.input('adduser', 'Web');
            request.input('clientcode', config.client.code);
            request.input('note', config.site.name);
            request.input('optinveh', 'Web');

            //EXECUTE STORED PROCEDURE
            request.execute('proc_optins_insert', err => {
                if(err) return callback(err);
                return callback(null, true)
            });
        });
    }
};
