'use strict';

const mssql = require('mssql');
const config = require('../config');
const Promise = require('bluebird');

// SET BLUEBIRD AS MSSQL PROMISE LIBRARY
mssql.promise = Promise;

// GLOBAL CONNECTION
let cp = null;

// CHECK IF CONNECTION EXISTS AND RETURN IT
function getConnection() {
    if (cp) return cp;
    return new Promise((resolve, reject) => {
        let conn = new mssql.Connection(config.sql, err => {
            if (err) {
                cp = null;
                return reject(err);
            }
            return resolve(conn);
        });
    });
}

module.exports = {

    // SIMPLIFY THE SQL QUERYSTRING
    query: (query, callback) => {

        if (typeof query !== 'string') throw new Error('The first parameter in "query" must be a string!');
        if (callback && typeof callback !== 'function') throw new Error('The second parameter in "query" must be a function!');

        // GET CONNECTION
        getConnection().then(conn => {

            // INIT REQUEST
            let request = new mssql.Request(conn);

            // EXECUTE QUERY
            request.query(query).then(recordset => {
                if (typeof callback === 'function') {
                    return callback(null, recordset);
                }

            })

            .catch(err => callback(err));

        })

        .catch(err => callback(err));

    },

    // ASYNCHRONOUS QUERY
    queryAsync: (query) => {

        if (typeof query !== 'string') throw new Error('The first parameter in "queryAsync" must be a string!');

        // GET CONNECTION
        return getConnection().then(conn => {

            // INIT REQUEST
            let request = new mssql.Request(conn);

            // RETURN EXECUTE PROMISE
            return request.query(query);

        })

        .catch(err => {
            throw err;
        });
    },

    // SIMPLIFY THE SQL STORED PROCEDURE
    storedProcedure: (spName, vars, callback) => {

        if (typeof spName !== 'string') throw new Error('The first parameter in "storedProcedure" must be a string!');

        // GET THE CONNECTION
        getConnection().then(conn => {

            // INIT REQUEST
            let request = new mssql.Request(conn);

            // LOOP THROUGH VARIABLES
            if(typeof vars ==='object'){
                for (let attributename in vars) {
                    if (vars.hasOwnProperty(attributename)) {

                        // IF THE VALUE THAT IS GETTING PASSED IN IS A DECIMAL, MAKE SURE WE PASS IN A DECIMAL DATA TYPE
                        if(typeof vars[attributename] === "number" && vars[attributename].toString().indexOf('.') > -1){
                            request.input(attributename, mssql.Decimal(14, 4), vars[attributename] );
                            continue;
                        }

                        request.input(attributename, vars[attributename]);
                    }
                }

            }else if(typeof vars ==='function'){
                callback = vars;
            }

            // EXECUTE
            request.execute(spName).then(recordset => {
                if (typeof callback === 'function') {
                    return callback(null, recordset);
                }

            })

            .catch(err => callback(err));

        })

        .catch(err => callback(err));

    },

    // ASYNCHRONOUS STORED PROCEDURE
    storedProcedureAsync: (spName, vars) => {

        // CHECK PARAMETERS FOR CORRECT TYPES
        if (typeof spName !== 'string') throw new Error('The first parameter in "storedProcedureAsync" must be a string!');
        if (vars && typeof vars !== 'object') throw new Error('The second parameter in "storedProcedureAsync" must be an object!');

        // GET CONNECTION
        return getConnection().then(conn => {

            // INIT REQUEST
            let request = new mssql.Request(conn);

            // LOOP THROUGH VARIABLES
            if (vars) {
                for (let attributename in vars) {
                    if (vars.hasOwnProperty(attributename)) {

                        // IF THE VALUE THAT IS GETTING PASSED IN IS A DECIMAL, MAKE SURE WE PASS IN A DECIMAL DATA TYPE
                        if(typeof vars[attributename] === "number" && vars[attributename].toString().indexOf('.') > -1){
                            request.input(attributename, mssql.Decimal(14, 4), vars[attributename] );
                            continue;
                        }

                        request.input(attributename, vars[attributename]);
                    }
                }
            }

            // RETURN EXECUTE PROMISE
            return request.execute(spName);

        })

        .catch(err => {
            throw err;
        });
    }

};