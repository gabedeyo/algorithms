'use strict';

module.exports = (req, res, next) => {
    let err = new Error();
    err.status = 404;
    err.reason = "page not found";

    res.status(err.status);
    res.render('errors/404');
};