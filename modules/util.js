'use strict';

module.exports = {


    //TRIMS EVERY VALUE WITHIN AN OBJECT
    deepTrim: (obj) => {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                let value = obj[prop], type = typeof value;
                if (value !== null && (type == "string" || type == "object") && obj.hasOwnProperty(prop)) {
                    if (type == "object") {
                        module.exports.deepTrim(obj[prop]);
                    } else {
                        obj[prop] = obj[prop].trim();
                    }
                }
            }
        }
        return obj;
    },



    formatPhone: (phone) => {
        phone = phone.replace(/\D/g,'');
        if(phone.length == 10) return phone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
        if(phone.length == 11) return phone.replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, '$1 $2-$3-$4');
        return phone;
    },



    //GET FILE EXTENSION
    fileExt: (filename) => filename.split('.').pop()

};