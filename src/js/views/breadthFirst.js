var bfsArray = [];

// Node Class
class Node {
  constructor(val) {
    this.value = val;
    this.left = null;
    this.right = null;
  }
}

// Binary Search Tree Class
class BinarySearchTree{
    constructor(){
        this.root = null;
    }

    push(val){
        var root = this.root;

        if(!root){
            this.root = new Node(val);
            return;
        }

        var currentNode = root;
        var newNode = new Node(val);

        while(currentNode){
            if(val < currentNode.value){
                if(!currentNode.left){
                    currentNode.left = newNode;
                    break;
                }else {
                    currentNode = currentNode.left;
                }
            }else{
                if(!currentNode.right){
                    currentNode.right = newNode;
                    break;
                }else{
                    currentNode = currentNode.right;
                }
            }
        }
    }

    /* Depth First Algorithms */

    // Pre Order Traversal
    preorder(node){
        if(node){
            preOrderArray.push(node.value);
            this.preorder(node.left);
            this.preorder(node.right);
        }
    }

    // In Order Traversal
    inorder(node){
        if(node){
            this.inorder(node.left);
            inOrderArray.push(node.value);
            this.inorder(node.right);
        }
    }

    // Post Order Traversal
    postorder(node){
        if(node){
            this.postorder(node.left);
            this.postorder(node.right);
            postOrderArray.push(node.value);
        }
    }



    /* Breadth First Algorithm */
    BreadthFirstSearch(root){

        if(root == null) return;

        var Q = new Queue();
        Q.enqueue(root);

        while(!Q.isEmpty()){
            var node = new Node();
            node = Q.dequeue();

            bfsArray.push(node.value);

            if(node.left != null){
                Q.enqueue(node.left);
            }
            if(node.right != null){
                Q.enqueue(node.right);
            }
        }
    }
}

class Queue{
    constructor(){
        this.queue = [];
        this.offset = 0;
    }

    getLength(){
        return (this.queue.length - this.offset);
    }

    isEmpty(){
        return (this.queue.length == 0);
    }

    enqueue(item){
        this.queue.push(item);
    }

    dequeue(){
        if(this.queue.length == 0) return undefined;

        var item = this.queue[this.offset];

        if(++ this.offset * 2 >= this.queue.length){
            this.queue = this.queue.slice(this.offset);
            this.offset = 0;
        }

        return item;
    }

    peek(){
        return (this.queue.length > 0 ? this.queue[this.offset] : undefined);
    }

    BreadthFirstSearch(Q){
        if(Q.isEmpty()) return;

        var bfsInfo = [];

        for(var i = 0; i < Q.getLength(); i++){
            bfsInfo[i] = {
                distance: null,
                predecessor: null
            };
        }

        //bfsInfo[bst.root];

        var node = new Node();
        node = Q.dequeue();

        bfsArray.push(node.value);

        if(node.left != null)
            Q.enqueue(node.left);
        if(node.right != null)
            Q.enqueue(node.right);

        this.BreadthFirstSearch(Q);
    }
}

/* On Load Function */
(function () {
    // Get Canvas Elements Here
    var canvas = document.getElementById('testCanvas');
    var ctx = drawInitial(canvas);
    var offset = -150;

    var myQueue = new Queue();

    var bst = new BinarySearchTree();

    drawHeading(ctx, "Binary Tree", 100, 200);
    for(let i = 0; i < 10; i++){
        var val = Math.floor(Math.random() * (100 - 0 + 1) + 0);
        bst.push(val);
        myQueue.enqueue(val);
        drawHeading(ctx, val, 100, offset);
        offset += 35;
    }

    bst.BreadthFirstSearch(bst.root);

    drawHeading(ctx, "Breadth First Search", -100, 200);
    offset = -150;
    for(let i = bfsArray.length; i > 0; i--){
        drawHeading(ctx, bfsArray[i-1], -100, offset)
        offset += 35;
    }

})();

//Draw Inital Canvas Elements
function drawInitial(canvas){
    let windowWidth = $(document).width();
    let windowHeight = $(document).height();

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = windowWidth / 3;
    ctx.canvas.height = windowHeight / 2;

    return ctx;
}

function drawHeading(ctx, printString, x, y){
    ctx.font="25px sans-serif";
    ctx.textAlign="center";

    ctx.fillText(printString, ctx.canvas.width/2 - x, ctx.canvas.height/2 - y);
}
