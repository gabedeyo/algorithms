'use strict';

const flash           = require('connect-flash');
const nos             = require('../modules/nos');
const LocalStrategy   = require('passport-local').Strategy;
const passport        = require('passport');



module.exports = app => {


    // =================================================
    // BASIC PASSPORT SETUP
    // =================================================
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());


    // =================================================
    // SERIALIZE USER
    // =================================================
    passport.serializeUser((user, done) => {
        done(null, user.loginsid); //THIS NEEDS TO MATCH LOGIN TABLE GUID
    });


    // =================================================
    // DESERIALIZE USER
    // =================================================
    passport.deserializeUser((loginsid, done) => {


        nos.sql.storedProcedure('proc_logins_deserialize', {loginsid: loginsid}, (err, data) => {
            if (err) return done(err);
            data = nos.util.deepTrim(data);
            let user = data[0][0];

            //user's rights are second table being returned
            user.rights = [];
            for(let i = 0; i < data[1].length; i++){
                user.rights.push(data[1][i].objname);
            }

            return done(null, user);
        });


    });


    // =================================================
    // LOCAL USER LOGIN SCRIPT
    // =================================================
    passport.use('local-login', new LocalStrategy({

        // by default, local strategy uses username and password, we will override with email
        usernameField : 'loginid',
        passwordField : 'password',
        loginpoint: 'Landing Page',
        passReqToCallback : true // allows us to pass back the entire request to the callback

    },(req, loginid, password, done) => { // callback with email and password from our form


        //SELECT USER
        nos.sql.storedProcedureAsync('proc_Ord_UserLogin', {
            loginid:loginid,
            password:password,
            IpAddress: req.ip || ''
        })

        .then(rows => {
            //NO USERS FOUND
            if (!rows.length) return done(null, false, req.flash('loginMessage', 'Incorrect User Name or Password!'));



            // all is well, return successful user
            return done(null, rows[0][0]);
        })

        .catch(err => done(err));


    }));

    return passport;

};