'use strict';

const err404 = require('./404');
const err500 = require('./500');

module.exports = app => {
    app.use(err500); //500 error
    app.use('*', err404); //404 error
};
