'use strict';

const ip = require("ip");

const config = {};

//SITE TESTER INFO
config.tester = {};
config.tester.email = '';
config.tester.name = '';

//CLIENT INFO
config.client = {};
config.client.name = 'IBI Data';
config.client.code = 'IB';


//WEB SERVER
config.webServer = {};
config.webServer.productionServers = ['12.30.67.105', '54.213.124.29'];
config.webServer.ip = ip.address();



//SITE INFO
config.site = {};
// NEED TO SET URL PATHS BEFORE PUTTING ON A SERVER
config.site.devSrc = 'https://dev2.ibidata.net';
config.site.devPath = ''; // '/reddiamond'
config.site.productionSrc = ''; // 'https://www.ibidata.net'
config.site.productionPath = ''; // '/reddiamond'
config.site.name = 'Node Starter';
config.site.title = '';
config.site.phone = '';
config.site.email = '';
config.site.emailname = '';
config.site.src = ((config.webServer.productionServers.indexOf(config.webServer.ip) > -1) ? config.site.productionSrc : false) ||
    ((config.webServer.ip === '12.30.67.110') ? config.site.devSrc : false) ||
    'http://localhost:3000';
config.site.url = ((config.webServer.productionServers.indexOf(config.webServer.ip) > -1) ? config.site.productionPath : false) ||
    ((config.webServer.ip === '12.30.67.110') ? config.site.devPath : false) ||
    '';



//DEBUG SETTINGS
config.debug = {};
config.debug.testmode = !!config.webServer.productionServers.indexOf(config.webServer.ip);


//SMTP CONNECTION
config.smtp = {};
config.smtp.host = 'mail2.ibidata.com';
config.smtp.ignoreTLS = false;
config.smtp.port = 465;
config.smtp.secure = true; //enable ssl
//config.smtp.auth = {}
//config.smtp.auth.user = ''
//config.smtp.auth.pass = ''


//SQL CONNECTION
config.sql = {};
config.sql.database = 'IB';
config.sql.user = 'IBwebUser';
config.sql.password = 'IBItotheMOON';
config.sql.server = (config.debug.testmode ? 'SQLDEV': 'SQL2005');
config.sql.port = '1203';


//AWS SETTINGS
config.aws = {};
config.aws.region = 'us-west-2';
config.aws.credentials = {};
config.aws.credentials.accessKeyId = 'AKIAISVBUIU2AWJ7MHTA';
config.aws.credentials.secretAccessKey = '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';


//FILE UPLOAD SETTINGS
config.upload = {};
config.upload.destination = 'tmp/';
config.upload.maxFileSize = 20; // in MB
config.upload.maxFiles = 10; // how many files to upload at a time
// config.upload.accept = ['jpg', 'png', 'gif']; // uncomment to filter out extensions


//WEB SETTINGS
config.web = {};
config.web.port = process.env.PORT || 3000;

module.exports = Object.freeze(config);