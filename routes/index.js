'use strict';

const config = require('../config');

module.exports = app => {


    // PLACE ANY PUBLIC ROUTES HERE
    //app.locals.site = config.site;

    //============================================
    // CATCH ALL ROUTE - CHECK IF LOGGED IN
    //============================================
    app.use(`${config.site.url}/`, (req, res, next) => {
        res.render('index');
    });

    // PLACE ANY PASSWORD PROTECTED ROUTES HERE


};
