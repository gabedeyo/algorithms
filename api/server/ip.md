#IP Address
>IP Can be used for various IP Address needs
>
>IP Uses the node-ip library. More information can be found on their [github](https://github.com/indutny/node-ip)

```javascript

    const ip = require('ip');
    ip.address();

    //output: 'Get Local Address' (also saved as a variable in config.js: config.webServer.ip)
```
