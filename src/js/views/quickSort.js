// Place Data Structure Classes

(function(){
    // Get Canvas Elements
    var canvas = document.getElementById('testCanvas');
    var ctx = drawInitial(canvas);

    // Do Algorithm
})();

//Draw Inital Canvas Elements
function drawInitial(canvas){
    let windowWidth = $(document).width();
    let windowHeight = $(document).height();

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = windowWidth / 3;
    ctx.canvas.height = windowHeight / 2;

    return ctx;
}

function drawHeading(ctx, printString, x, y){
    ctx.font="25px sans-serif";
    ctx.textAlign="center";

    ctx.fillText(printString, ctx.canvas.width/2 - x, ctx.canvas.height/2 - y);
}
