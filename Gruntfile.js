'use strict';



module.exports = grunt => {

  require('time-grunt')(grunt);


  // Checks to make sure the public file path exists. If it doesn't, this will create it.
  // Right after you clone this repo, some public directories don't exist yet and grunt will error out without this function.
  function fileCheck(path) {
    if (!grunt.file.exists(path)) grunt.file.write(path);
    return path;
  }

  // Checks to make sure the src/js/views directory exists. If it doesn't, this will create it.
  // This directory is empty by default, so git does not copy it over. Grunt will error out without it.
  function dirCheck(path) {
    if (!grunt.file.isDir(path)) grunt.file.mkdir(path);
    return path;
  }

  // Dynamically creates the 'files' object for the Browserify task
  // This will read the src/js/views directory and create a public js file to correspond with it (it also concats the main.js file first)
  function getJsFiles(production) {
    let obj = {};
    grunt.file.recurse(dirCheck('src/js/views'), (abspath, rootdir, subdir, filename) => {
      obj[fileCheck(`public/assets/js/${filename.replace('.js', '.min.js')}`)] = ['src/js/main.js', abspath];
      if (production) obj[fileCheck(`public/assets/js/${filename}`)] = ['src/js/main.js', abspath];
    });
    if (Object.keys(obj).length > 0) return obj;
    else {
      obj[fileCheck('public/assets/js/main.min.js')] = ['src/js/main.js'];
      if (production) obj[fileCheck('public/assets/js/main.js')] = ['src/js/main.js'];
      return obj;
    }
  }

  // Dynamically creates the 'files' object for JS Uglify task
  // This will read the public/assets/js directory, looking for .min.js files to minify.
  function minJsFiles() {
    let obj = {};
    grunt.file.recurse('public/assets/js', (abspath, rootdir, subdir, filename) => {
      if (filename.indexOf('.min.js') > -1) {
        obj[`public/assets/js/${filename}`] = `public/assets/js/${filename}`;
      }
    });
    return obj;
  }

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),


    browserify: {
      options: {
        transform: [
          ['babelify', {
            ignore: ['node_modules/**', 'src/js/plugins'],
            presets: ['es2015']
          }]
        ]
      },
      production: {
        files: getJsFiles(true)
      },
      dev: {
        files: getJsFiles(false)
      }
    },



    // Only runs in production
    uglify: {
      production: {
        files: minJsFiles()
      }
    },


    less: {
      development: {
        options: {
          compress: false,
          yuicompress: false,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          'public/assets/css/main.css': 'src/less/main.less'
        }
      }
    },

    // Only runs in production
    cssmin: {
      dev: {
        files: {
          'public/assets/css/main.min.css': ['public/assets/css/main.min.css']
        }
      }
    },


    watch: {
      options: {
        livereload: true,
        spawn: false
      },

      express: {
        files: ['app.js', 'middleware/**', 'routes/**', 'views/**', 'modules/**', 'config.js', '!node_modules/**'],
        tasks: ['express:dev']
      },

      // Disable livereload and spawn the less task
      // This is what allows hot-reloading
      less: {
        options: {
          livereload: false,
          spawn: true
        },
        files: ['src/less/**'],
        tasks: ['build-less']
      },

      // Watch the css file to trigger hot-reloading
      // We don't need any tasks because we are not reloading
      css: {
        files: ['public/assets/css/main.min.css'],
        tasks: []
      },

      js: {
        files: ['src/js/**/*.js'],
        tasks: ['build-js-dev']
      },

      html: {
        files: ['public/**/*.html']
      }

    },

    express: {
      dev: {
        options: {
          script: 'app.js',

          // Set the node_env to development
          // This will trigger the livereload injection middleware
          node_env: 'development'
        }
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: ['last 5 versions']
          })
        ]
      },
      dev: {
        src: 'public/assets/css/main.css',
        dest: 'public/assets/css/main.min.css'
      }
    }

  });


  // ==================================================================================
  // TASKS
  // ==================================================================================


  // BUILDS JS FILE IN DEVELOPMENT - LIVERELOAD TASK
  grunt.registerTask('build-js-dev', [], () => {
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.task.run('newer:browserify:dev');
  });

  // BUILDS JS FILE IN DEVELOPMENT - DEFAULT TASK
  grunt.registerTask('build-js-dev-default', [], () => {
    grunt.loadNpmTasks('grunt-browserify');
    grunt.task.run('browserify:dev');
  });

  // BUILDS JS FILE IN PRODUCTION
  grunt.registerTask('build-js-production', [], () => {
    grunt.loadNpmTasks('grunt-browserify');
    grunt.task.run('browserify:production');
  });

  // BUILDS CSS FILE AND ADDS PREFIXES
  grunt.registerTask('build-less', [], () => {
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.task.run('less', 'postcss:dev');
  });

  // MINIFY JS & CSS (ONLY IN PRODUCTION)
  grunt.registerTask('min', [], () => {
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.task.run('cssmin', 'uglify');
  });

  // START SERVER AND WATCH
  grunt.registerTask('server', [], () => {
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.task.run('express:dev', 'watch');
  });

  // PRODUCTION TASK
  grunt.registerTask('production', ['build-js-production', 'build-less', 'min']);

  // DEFAULT DEVELOPMENT TASK
  grunt.registerTask('default', [], () => {
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.task.run('build-js-dev-default', 'build-less', 'express', 'watch');
  });


};
