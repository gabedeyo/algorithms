'use strict';

module.exports = (req, res, next) => {
    res.locals.user = req.user;
    res.locals.config = require('../config');
    res.locals.isInternal = req.isInternal;
    next();
};